#
# Zander Brown <zbrown@gnome.org>, 2021.
# Andi Chandler <andi@gowling.com>, 2024.
# Bruce Cowan <bruce@bcowan.me.uk>, 2024.
#
msgid ""
msgstr ""
"Project-Id-Version: Apostrophe\n"
"Report-Msgid-Bugs-To: https://gitlab.gnome.org/World/apostrophe/issues\n"
"POT-Creation-Date: 2024-04-17 20:12+0000\n"
"PO-Revision-Date: 2024-04-18 13:22+0100\n"
"Last-Translator: Bruce Cowan <bruce@bcowan.me.uk>\n"
"Language-Team: English - United Kingdom <en_GB@li.org>\n"
"Language: en_GB\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"
"X-Generator: Poedit 3.4.2\n"

#: data/org.gnome.gitlab.somas.Apostrophe.desktop.in.in:3
#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:4
msgid "Apostrophe"
msgstr "Apostrophe"

#. Translators: Search terms to find this application. Do NOT translate or localize the semicolons! Do NOT translate "uberwriter" or "apostrophe"! The list MUST also end with a semicolon!
#: data/org.gnome.gitlab.somas.Apostrophe.desktop.in.in:5
msgid "uberwriter;UberWriter;apostrophe;markdown;editor;text;write;"
msgstr "uberwriter;UberWriter;apostrophe;markdown;editor;text;write;"

#: data/org.gnome.gitlab.somas.Apostrophe.desktop.in.in:6
msgid "Apostrophe, an elegant, distraction-free markdown editor"
msgstr "Apostrophe, an elegant, distraction-free markdown editor"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:35
msgid "Color scheme"
msgstr "Colour scheme"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:36
msgid "Use the color scheme in the application's UI and in the text area."
msgstr "Use the colour scheme in the application's UI and in the text area."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:42
msgid "Check spelling while typing"
msgstr "Check spelling while typing"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:43
msgid "Enable or disable spellchecking."
msgstr "Enable or disable spellchecking."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:49
msgid "Synchronize editor/preview scrolling"
msgstr "Synchronise editor/preview scrolling"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:50
msgid "Keep the editor and preview scroll positions in sync."
msgstr "Keep the editor and preview scroll positions in sync."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:56
msgid "Input format"
msgstr "Input format"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:57
msgid "Input format to use when previewing and exporting using Pandoc."
msgstr "Input format to use when previewing and exporting using Pandoc."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:63
msgid "Autohide Headerbar"
msgstr "Autohide Headerbar"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:64
msgid "Hide the header and status bars when typing."
msgstr "Hide the header and status bars when typing."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:70
msgid "Open file base path"
msgstr "Open file base path"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:71
msgid "Open file paths of the current session"
msgstr "Open file paths of the current session"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:77
msgid "Default statistic"
msgstr "Default statistic"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:78
msgid "Which statistic is shown on the main window."
msgstr "Which statistic is shown on the main window."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:84
msgid "Characters per line"
msgstr "Characters per line"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:85
msgid "Maximum number of characters per line within the editor."
msgstr "Maximum number of characters per line within the editor."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:91
#: data/ui/AboutHemingway.ui:6
msgid "Hemingway Mode"
msgstr "Hemingway Mode"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:92
msgid "Whether the user can delete text or not."
msgstr "Whether the user can delete text or not."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:98
msgid "Hemingway Toast Count"
msgstr "Hemingway Toast Count"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:99
msgid "Number of times the Hemingway Toast has been shown"
msgstr "Number of times the Hemingway Toast has been shown"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:105
msgid "Preview mode"
msgstr "Preview mode"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:106
msgid "How to display the preview."
msgstr "How to display the preview."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:112
#: data/ui/Preferences.ui:64
#| msgid "Preview active"
msgid "Preview Security"
msgstr "Preview Security"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:113
msgid "Security policy to apply when rendering files."
msgstr "Security policy to apply when rendering files."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:119
msgid "Preview active"
msgstr "Preview active"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:120
msgid "Whether showing of preview is active when launching a new window."
msgstr "Whether showing of preview is active when launching a new window."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:126
msgid "Text size"
msgstr "Text size"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:127
msgid "Preferred relative size for the text."
msgstr "Preferred relative size for the text."

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:133
msgid "Toolbar"
msgstr "Toolbar"

#: data/org.gnome.gitlab.somas.Apostrophe.gschema.xml:134
msgid "Whether the toolbar is shown or not."
msgstr "Whether the toolbar is shown or not."

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:5
msgid "Edit Markdown in style"
msgstr "Edit Markdown in style"

#. developer_name tag deprecated with Appstream 1.0
#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:9
msgid "Manuel Genovés"
msgstr "Manuel Genovés"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:14
msgid "Focus on your writing with a clean, distraction-free markdown editor."
msgstr "Focus on your writing with a clean, distraction-free markdown editor."

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:15
msgid "Features:"
msgstr "Features:"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:17
msgid "An UI tailored to comfortable writing"
msgstr "An UI tailored to comfortable writing"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:18
msgid "A distraction-free mode"
msgstr "A distraction-free mode"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:19
msgid "Dark, light and sepia themes"
msgstr "Dark, light and sepia themes"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:20
msgid ""
"Everything you expect from a text editor, such as spellchecking or document "
"statistics"
msgstr ""
"Everything you expect from a text editor, such as spellchecking or document "
"statistics"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:21
msgid "Live preview of what you write"
msgstr "Live preview of what you write"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:22
msgid ""
"Export to all kind of formats: PDF, Word/Libreoffice, LaTeX, or even HTML "
"slideshows"
msgstr ""
"Export to all kind of formats: PDF, Word/Libreoffice, LaTeX, or even HTML "
"slideshows"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:41
msgid "Main window"
msgstr "Main window"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:45
msgid "Main window in dark mode"
msgstr "Main window in dark mode"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:49
msgid "Inserting a table with the help of the toolbar"
msgstr "Inserting a table with the help of the toolbar"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:53
msgid "The preview lets you see a live rendered version of your document"
msgstr "The preview lets you see a live rendered version of your document"

#: data/org.gnome.gitlab.somas.Apostrophe.metainfo.xml.in.in:57
msgid "The focus mode allow for a more distraction-free experience"
msgstr "The focus mode allow for a more distraction-free experience"

#: data/ui/About.ui.in:9
msgid "Copyright (C) 2022, Manuel G., Wolf V."
msgstr "Copyright (C) 2022, Manuel G., Wolf V."

#. Put your name in here, like this:
#. Manuel Genovés <manuel.genoves@gmail.com>
#: data/ui/About.ui.in:17
msgid "translator-credits"
msgstr ""
"Zander Brown <zbrown@gnome.org>\n"
"Andi Chandler <andi@gowling.com>\n"
"Bruce Cowan <bruce@bcowan.me.uk>"

#: data/ui/AboutHemingway.ui:7
msgid ""
"The Hemingway Mode mimics the experience of writing on a typewriter, not "
"allowing you to delete any text or doing any kind of editing. \n"
"\n"
"Only being able to move forward may be frustrating, but can be an "
"interesting exercise for improving your writing skills, gaining focus or "
"facing the task on hand with a different mindset.\n"
"  "
msgstr ""
"The Hemingway Mode mimics the experience of writing on a typewriter, not "
"allowing you to delete any text or doing any kind of editing. \n"
"\n"
"Only being able to move forward may be frustrating, but can be an "
"interesting exercise for improving your writing skills, gaining focus or "
"facing the task on hand with a different mindset.\n"
"  "

#: data/ui/AboutHemingway.ui:13
msgid "_Close"
msgstr "_Close"

#: data/ui/Export.ui:21
msgid "Formats"
msgstr "Formats"

#: data/ui/Export.ui:67 apostrophe/export_dialog.py:126
#: apostrophe/export_dialog.py:328 apostrophe/export_dialog.py:332
msgid "Export"
msgstr "Export"

#: data/ui/Export.ui:86
msgid "Options"
msgstr "Options"

#: data/ui/Export.ui:89
msgid "Standalone"
msgstr "Stand-alone"

#: data/ui/Export.ui:90
msgid ""
"Use a header and footer to include things like stylesheets and meta "
"information"
msgstr ""
"Use a header and footer to include things like stylesheets and meta "
"information"

#: data/ui/Export.ui:96
msgid "Table of Contents"
msgstr "Table of Contents"

#: data/ui/Export.ui:102
msgid "Number Sections"
msgstr "Number Sections"

#: data/ui/Export.ui:112 data/ui/Export.ui:115 data/ui/Export.ui:126
#: data/ui/Export.ui:130
msgid "Page Size"
msgstr "Page Size"

#: data/ui/Export.ui:141
msgid "HTML Options"
msgstr "HTML Options"

#: data/ui/Export.ui:144
msgid "Self-Contained"
msgstr "Self-Contained"

#: data/ui/Export.ui:145
msgid "Produces an HTML file with no external dependencies"
msgstr "Produces an HTML file with no external dependencies"

#: data/ui/Export.ui:154
msgid "Syntax Highlighting"
msgstr "Syntax Highlighting"

#: data/ui/Export.ui:157
msgid "Use Syntax Highlighting"
msgstr "Use Syntax Highlighting"

#: data/ui/Export.ui:162
msgid "Highlight Style"
msgstr "Highlight Style"

#: data/ui/Export.ui:175
msgid "Presentation"
msgstr "Presentation"

#: data/ui/Export.ui:178
msgid "Incremental Bullets"
msgstr "Incremental Bullets"

#: data/ui/Export.ui:179
msgid "Show one bullet point after another in a slideshow"
msgstr "Show one bullet point after another in a slideshow"

#: data/ui/Headerbar.ui:10 apostrophe/main_window.py:772
#: apostrophe/main_window.py:790
msgid "New File"
msgstr "New File"

#: data/ui/Headerbar.ui:17
msgid "_Open"
msgstr "_Open"

#: data/ui/Headerbar.ui:19
#| msgid "Open a Markdown File"
msgid "Open Markdown File"
msgstr "Open Markdown File"

#: data/ui/Headerbar.ui:25
msgid "_Save"
msgstr "_Save"

#: data/ui/Headerbar.ui:27
#| msgid "Save the Markdown File"
msgid "Save Markdown File"
msgstr "Save Markdown File"

#: data/ui/Headerbar.ui:43
#| msgid "Open the Main Menu"
msgid "Main Menu"
msgstr "Main Menu"

#: data/ui/Headerbar.ui:54
msgid "Search in the File"
msgstr "Search in the File"

#: data/ui/Headerbar.ui:77
msgid "_Hemingway Mode"
msgstr "_Hemingway Mode"

#: data/ui/Headerbar.ui:81
msgid "_Focus Mode"
msgstr "_Focus Mode"

#: data/ui/Headerbar.ui:87
msgid "Find and _Replace"
msgstr "Find and _Replace"

#: data/ui/Headerbar.ui:93
msgid "_Preferences"
msgstr "_Preferences"

#: data/ui/Headerbar.ui:99
msgid "Open _Tutorial"
msgstr "Open _Tutorial"

#: data/ui/Headerbar.ui:104
msgid "_Keyboard Shortcuts"
msgstr "_Keyboard Shortcuts"

#: data/ui/Headerbar.ui:108
msgid "_About Apostrophe"
msgstr "_About Apostrophe"

#: data/ui/Headerbar.ui:119
msgid "_Save As..."
msgstr "_Save As..."

#: data/ui/Headerbar.ui:142
msgid "Advanced _Export…"
msgstr "Advanced _Export…"

#: data/ui/Headerbar.ui:148
msgid "_Copy HTML"
msgstr "_Copy HTML"

#: data/ui/Preferences.ui:12 data/ui/Preferences.ui:13
#| msgid "Check Spelling While Typing"
msgid "Check Spelling"
msgstr "Check Spelling"

#: data/ui/Preferences.ui:18
msgid "Auto-Hide Header Bar"
msgstr "Auto-Hide Header Bar"

#: data/ui/Preferences.ui:19
msgid "Auto-hide header and status bars while typing"
msgstr "Auto-hide header and status bars while typing"

#: data/ui/Preferences.ui:24
msgid "Large Text"
msgstr "Large Text"

#: data/ui/Preferences.ui:25
msgid "Reduce the margins width and increase the text size when possible"
msgstr "Reduce the margins width and increase the text size when possible"

#: data/ui/Preferences.ui:31
msgid "Restore Session"
msgstr "Restore Session"

#: data/ui/Preferences.ui:32
msgid "Return to your previous session when Apostrophe is started"
msgstr "Return to your previous session when Apostrophe is started"

#: data/ui/Preferences.ui:37
msgid "Input Format"
msgstr "Input Format"

#: data/ui/Preferences.ui:38
msgid "Flavor of markdown Apostrophe will use"
msgstr "Flavour of markdown Apostrophe will use"

#: data/ui/Preferences.ui:47
msgid "Markdown Flavor Documentation"
msgstr "Markdown Flavour Documentation"

#: data/ui/Preferences.ui:65
msgid "Desired level of security when rendering the preview"
msgstr "Desired level of security when rendering the preview"

#: data/ui/PreviewLayoutSwitcher.ui:7 apostrophe/preview_handler.py:205
#: apostrophe/preview_window.py:32
msgid "Preview"
msgstr "Preview"

#: data/ui/PreviewLayoutSwitcher.ui:24
msgid "Screen Layout"
msgstr "Screen Layout"

#: data/ui/Recents.ui:39
msgid "No Recent Documents"
msgstr "No Recent Documents"

#: data/ui/Recents.ui:63
msgid "No Results Found"
msgstr "No Results Found"

#: data/ui/SearchBar.ui:29
msgid "Previous Match"
msgstr "Previous Match"

#: data/ui/SearchBar.ui:39
msgid "Next Match"
msgstr "Next Match"

#. Translators: This indicates case sensitivity, so it should be the first vowel of the language in lowercase and uppercase
#: data/ui/SearchBar.ui:55
msgid "aA"
msgstr "aA"

#: data/ui/SearchBar.ui:58
msgid "Case Sensitive"
msgstr "Case Sensitive"

#: data/ui/SearchBar.ui:66
msgid "Regular Expression"
msgstr "Regular Expression"

#: data/ui/SearchBar.ui:73 data/ui/SearchBar.ui:111
msgid "Replace"
msgstr "Replace"

#: data/ui/SearchBar.ui:120
msgid "Replace All"
msgstr "Replace All"

#: data/ui/Shortcuts.ui:11
msgctxt "shortcut window"
msgid "General"
msgstr "General"

#: data/ui/Shortcuts.ui:14
msgctxt "shortcut window"
msgid "New"
msgstr "New"

#: data/ui/Shortcuts.ui:20
msgctxt "shortcut window"
msgid "Open"
msgstr "Open"

#: data/ui/Shortcuts.ui:26
msgctxt "shortcut window"
msgid "Save"
msgstr "Save"

#: data/ui/Shortcuts.ui:32
msgctxt "shortcut window"
msgid "Save As"
msgstr "Save As"

#: data/ui/Shortcuts.ui:38
msgctxt "shortcut window"
msgid "Close Document"
msgstr "Close Document"

#: data/ui/Shortcuts.ui:44
msgctxt "shortcut window"
msgid "Show Preferences"
msgstr "Show Preferences"

#: data/ui/Shortcuts.ui:50
msgctxt "shortcut window"
msgid "Keyboard Shortcuts"
msgstr "Keyboard Shortcuts"

#: data/ui/Shortcuts.ui:56
msgctxt "shortcut window"
msgid "Quit Application"
msgstr "Quit Application"

#: data/ui/Shortcuts.ui:64
msgctxt "shortcut window"
msgid "Modes"
msgstr "Modes"

#: data/ui/Shortcuts.ui:67
msgctxt "shortcut window"
msgid "Focus Mode"
msgstr "Focus Mode"

#: data/ui/Shortcuts.ui:73
msgctxt "shortcut window"
msgid "Hemingway Mode"
msgstr "Hemingway Mode"

#: data/ui/Shortcuts.ui:80
msgctxt "shortcut window"
msgid "Preview"
msgstr "Preview"

#: data/ui/Shortcuts.ui:86
msgctxt "shortcut window"
msgid "Fullscreen"
msgstr "Fullscreen"

#: data/ui/Shortcuts.ui:94 data/ui/Shortcuts.ui:97
msgctxt "shortcut window"
msgid "Find"
msgstr "Find"

#: data/ui/Shortcuts.ui:103
msgctxt "shortcut window"
msgid "Find and Replace"
msgstr "Find and Replace"

#: data/ui/Shortcuts.ui:111
msgctxt "shortcut window"
msgid "Markdown"
msgstr "Markdown"

#: data/ui/Shortcuts.ui:114
msgctxt "shortcut window"
msgid "Separator"
msgstr "Separator"

#: data/ui/Shortcuts.ui:120
msgctxt "shortcut window"
msgid "Header"
msgstr "Header"

#: data/ui/Shortcuts.ui:126
msgctxt "shortcut window"
msgid "List Item"
msgstr "List Item"

#: data/ui/Shortcuts.ui:133
msgctxt "shortcut window"
msgid "Italic"
msgstr "Italic"

#: data/ui/Shortcuts.ui:139
msgctxt "shortcut window"
msgid "Bold"
msgstr "Bold"

#: data/ui/Shortcuts.ui:145
msgctxt "shortcut window"
msgid "Strikeout"
msgstr "Strikeout"

#: data/ui/Shortcuts.ui:153
msgctxt "shortcut window"
msgid "Copy and Paste"
msgstr "Copy and Paste"

#: data/ui/Shortcuts.ui:156
msgctxt "shortcut window"
msgid "Copy"
msgstr "Copy"

#: data/ui/Shortcuts.ui:162
msgctxt "shortcut window"
msgid "Cut"
msgstr "Cut"

#: data/ui/Shortcuts.ui:168
msgctxt "shortcut window"
msgid "Paste"
msgstr "Paste"

#: data/ui/Shortcuts.ui:176
msgctxt "shortcut window"
msgid "Undo and Redo"
msgstr "Undo and Redo"

#: data/ui/Shortcuts.ui:179
msgctxt "shortcut window"
msgid "Undo Previous Command"
msgstr "Undo Previous Command"

#: data/ui/Shortcuts.ui:185
msgctxt "shortcut window"
msgid "Redo Previous Command"
msgstr "Redo Previous Command"

#: data/ui/Shortcuts.ui:193
msgctxt "shortcut window"
msgid "Selection"
msgstr "Selection"

#: data/ui/Shortcuts.ui:196
msgctxt "shortcut window"
msgid "Select All Text"
msgstr "Select All Text"

#: data/ui/Statsbar.ui:21 data/ui/Statsbar.ui:40
msgid "Show Statistics"
msgstr "Show Statistics"

#: data/ui/Statsbar.ui:22
msgid "0 Words"
msgstr "0 Words"

#: data/ui/TexliveWarning.ui:25 data/ui/TexliveWarning.ui:79
msgid "TexLive Required"
msgstr "TexLive Required"

#: data/ui/TexliveWarning.ui:33
msgid ""
"Apostrophe needs the TeXLive extension\n"
"in order to export PDF or LaTeX files.\n"
"\n"
"Install it from Apostrophe's page in Software\n"
"or by running the following command in a terminal:"
msgstr ""
"Apostrophe needs the TeXLive extension\n"
"in order to export PDF or LaTeX files.\n"
"\n"
"Install it from Apostrophe's page in Software\n"
"or by running the following command in a terminal:"

#: data/ui/TexliveWarning.ui:53
msgid "Copy to Clipboard"
msgstr "Copy to Clipboard"

#: data/ui/TexliveWarning.ui:87
msgid ""
"Apostrophe needs TeXLive\n"
"in order to export PDF or LaTeX files.\n"
"\n"
"Install it from your distribution repositories."
msgstr ""
"Apostrophe needs TeXLive\n"
"in order to export PDF or LaTeX files.\n"
"\n"
"Install it from your distribution repositories."

#: data/ui/ThemeSwitcher.ui:17
msgid "Use System Colors"
msgstr "Use System Colours"

#: data/ui/ThemeSwitcher.ui:27
msgid "Use Light Colors"
msgstr "Use Light Colours"

#: data/ui/ThemeSwitcher.ui:37
msgid "Use Sepia Colors"
msgstr "Use Sepia Colours"

#: data/ui/ThemeSwitcher.ui:47
msgid "Use Dark Colors"
msgstr "Use Dark Colours"

#: data/ui/Toolbar.ui:27
#| msgctxt "shortcut window"
#| msgid "Bold"
msgid "Bold"
msgstr "Bold"

#: data/ui/Toolbar.ui:37
#| msgctxt "shortcut window"
#| msgid "Italic"
msgid "Italic"
msgstr "Italic"

#: data/ui/Toolbar.ui:47
#| msgid "strikethrough text"
msgid "Strikethrough"
msgstr "Strikethrough"

#: data/ui/Toolbar.ui:57
#| msgid "Header"
msgid "Heading"
msgstr "Heading"

#: data/ui/Toolbar.ui:68
msgid "Unordered List"
msgstr "Unordered List"

#: data/ui/Toolbar.ui:78
msgid "Ordered List"
msgstr "Ordered List"

#: data/ui/Toolbar.ui:88
msgid "Check List"
msgstr "Tickbox List"

#: data/ui/Toolbar.ui:98
msgid "Blockquote"
msgstr "Blockquote"

#: data/ui/Toolbar.ui:108
msgid "Code"
msgstr "Code"

#: data/ui/Toolbar.ui:118
msgid "Link"
msgstr "Link"

#: data/ui/Toolbar.ui:128 apostrophe/text_view_format_inserter.py:435
msgid "Image"
msgstr "Image"

#: data/ui/Toolbar.ui:138
msgid "Table"
msgstr "Table"

#: data/ui/Toolbar.ui:153
msgid "Show More Controls"
msgstr "Show More Controls"

#: data/ui/Window.ui:64
msgid "This file may be insecure"
msgstr "This file may be insecure"

#: data/ui/Window.ui:65
msgid ""
"Previewing files from untrusted sources can be dangerous.\n"
"If you're unsure about the contents of this file, open it in the Restricted "
"Preview where javascript and HTML are disabled."
msgstr ""
"Previewing files from untrusted sources can be dangerous.\n"
"If you're unsure about the contents of this file, open it in the Restricted "
"Preview where javascript and HTML are disabled."

#: data/ui/Window.ui:77
#| msgid "Preview"
msgid "Load Preview"
msgstr "Load Preview"

#: data/ui/Window.ui:86
msgid "Load Restricted Preview"
msgstr "Load Restricted Preview"

#: data/ui/Window.ui:159
msgid "File Has Changed on Disk"
msgstr "File Has Changed on Disk"

#: data/ui/Window.ui:169
msgid "The file has been changed by another program"
msgstr "The file has been changed by another program"

#: data/ui/Window.ui:177
msgid "_Discard Changes and Reload"
msgstr "_Discard Changes and Reload"

#: apostrophe/application.py:228
msgid "Donate"
msgstr "Donate"

#: apostrophe/application.py:229
msgid "Translations"
msgstr "Translations"

#: apostrophe/export_dialog.py:121 apostrophe/helpers.py:84
msgid "Close"
msgstr "Close"

#: apostrophe/export_dialog.py:129 apostrophe/export_dialog.py:333
#, python-format
msgid "Export to %s"
msgstr "Export to “%s”"

#: apostrophe/export_dialog.py:131 apostrophe/export_dialog.py:329
#: apostrophe/export_dialog.py:334 apostrophe/main_window.py:416
#: apostrophe/main_window.py:494 apostrophe/main_window.py:587
msgid "Cancel"
msgstr "Cancel"

#: apostrophe/export_dialog.py:155 apostrophe/export_dialog.py:322
#, python-brace-format
msgid ""
"An error happened while trying to export:\n"
"\n"
"{err_msg}"
msgstr ""
"Unable to export:\n"
"\n"
"{err_msg}"

#: apostrophe/export_dialog.py:235
msgid "Export to {}"
msgstr "Export to {}"

#: apostrophe/export_dialog.py:329
msgid "Select folder"
msgstr "Select Folder"

#: apostrophe/helpers.py:83
msgid "Error"
msgstr "Error"

#: apostrophe/inhibitor.py:36
msgid "Unsaved documents"
msgstr "Unsaved documents"

#: apostrophe/inline_preview.py:201
msgid "Formula looks incorrect:"
msgstr "Formula looks incorrect:"

#: apostrophe/inline_preview.py:241
msgid "No matching footnote found"
msgstr "No matching footnote found"

#: apostrophe/inline_preview.py:261
msgid "noun"
msgstr "noun"

#: apostrophe/inline_preview.py:263
msgid "verb"
msgstr "verb"

#: apostrophe/inline_preview.py:265
msgid "adjective"
msgstr "adjective"

#: apostrophe/inline_preview.py:267
msgid "adverb"
msgstr "adverb"

#. Hemingway Toast
#: apostrophe/main_window.py:153
msgid "Text can't be deleted while on Hemingway mode"
msgstr "Text can't be deleted while on Hemingway mode"

#: apostrophe/main_window.py:156
msgid "Tell me more"
msgstr "Tell me more"

#: apostrophe/main_window.py:412
msgid "Save your File"
msgstr "Save your file"

#: apostrophe/main_window.py:415 apostrophe/main_window.py:589
msgid "Save"
msgstr "Save"

#: apostrophe/main_window.py:483
msgid "Markdown Files"
msgstr "Markdown Files"

#: apostrophe/main_window.py:487
msgid "Plain Text Files"
msgstr "Plain Text Files"

#: apostrophe/main_window.py:490
msgid "Open a .md file"
msgstr "Open a .md file"

#: apostrophe/main_window.py:493
msgid "Open"
msgstr "Open"

#: apostrophe/main_window.py:581
msgid "Save Changes?"
msgstr "Save Changes?"

#: apostrophe/main_window.py:582
#, python-format
msgid ""
"“%s” contains unsaved changes. If you don’t save, all your changes will be "
"permanently lost."
msgstr ""
"“%s” contains unsaved changes. If you don’t save, all your changes will be "
"permanently lost."

#: apostrophe/main_window.py:588
msgid "Discard"
msgstr "Discard"

#: apostrophe/preferences_dialog.py:97
msgid "Always Ask"
msgstr "Always Ask"

#: apostrophe/preferences_dialog.py:101
msgid "Always Use Restricted Preview"
msgstr "Always Use Restricted Preview"

#: apostrophe/preferences_dialog.py:105
msgid "Always Use Unrestricted Preview"
msgstr "Always Use Unrestricted Preview"

#: apostrophe/preview_layout_switcher.py:41
msgid "Full-Width"
msgstr "Full-Width"

#: apostrophe/preview_layout_switcher.py:43
msgid "Half-Width"
msgstr "Half-Width"

#: apostrophe/preview_layout_switcher.py:45
msgid "Half-Height"
msgstr "Half-Height"

#: apostrophe/preview_layout_switcher.py:47
msgid "Windowed"
msgstr "Windowed"

#: apostrophe/stats_handler.py:122
msgid "{:n} of {:n} Characters"
msgstr "{:n} of {:n} Characters"

#: apostrophe/stats_handler.py:124
msgid "{:n} Character"
msgid_plural "{:n} Characters"
msgstr[0] "{:n} Character"
msgstr[1] "{:n} Characters"

#: apostrophe/stats_handler.py:127
msgid "{:n} of {:n} Words"
msgstr "{:n} of {:n} Words"

#: apostrophe/stats_handler.py:129
msgid "{:n} Word"
msgid_plural "{:n} Words"
msgstr[0] "{:n} Word"
msgstr[1] "{:n} Words"

#: apostrophe/stats_handler.py:132
msgid "{:n} of {:n} Sentences"
msgstr "{:n} of {:n} Sentences"

#: apostrophe/stats_handler.py:134
msgid "{:n} Sentence"
msgid_plural "{:n} Sentences"
msgstr[0] "{:n} Sentence"
msgstr[1] "{:n} Sentences"

#: apostrophe/stats_handler.py:137
msgid "{:n} of {:n} Paragraphs"
msgstr "{:n} of {:n} Paragraphs"

#: apostrophe/stats_handler.py:139
msgid "{:n} Paragraph"
msgid_plural "{:n} Paragraphs"
msgstr[0] "{:n} Paragraph"
msgstr[1] "{:n} Paragraphs"

#: apostrophe/stats_handler.py:142
msgid "{:d}:{:02d}:{:02d} of {:d}:{:02d}:{:02d} Read Time"
msgstr "{:d}:{:02d}:{:02d} of {:d}:{:02d}:{:02d} Read Time"

#: apostrophe/stats_handler.py:144
msgid "{:d}:{:02d}:{:02d} Read Time"
msgstr "{:d}:{:02d}:{:02d} Read Time"

#: apostrophe/text_view_format_inserter.py:24
msgid "italic text"
msgstr "italic text"

#: apostrophe/text_view_format_inserter.py:29
msgid "bold text"
msgstr "bold text"

#: apostrophe/text_view_format_inserter.py:34
msgid "strikethrough text"
msgstr "strikethrough text"

#: apostrophe/text_view_format_inserter.py:49
#: apostrophe/text_view_format_inserter.py:117
#: apostrophe/text_view_format_inserter.py:187
msgid "Item"
msgstr "Item"

#: apostrophe/text_view_format_inserter.py:258
msgid "Header"
msgstr "Header"

#: apostrophe/text_view_format_inserter.py:329
msgid "Quote"
msgstr "Quote"

#: apostrophe/text_view_format_inserter.py:371
msgid "code"
msgstr "code"

#: apostrophe/text_view_format_inserter.py:394
msgid "https://www.example.com"
msgstr "https://www.example.com"

#: apostrophe/text_view_format_inserter.py:395
msgid "link text"
msgstr "link text"

#: apostrophe/text_view_format_inserter.py:429
#: apostrophe/text_view_format_inserter.py:461
msgid "image caption"
msgstr "image caption"

#: apostrophe/text_view_format_inserter.py:441
msgid "Select an image"
msgstr "Select an image"

#: apostrophe/text_view_format_inserter.py:550
msgid ""
"\n"
"code\n"
msgstr ""
"\n"
"code\n"

#: apostrophe/text_view.py:179 apostrophe/text_view.py:181
msgid "web page"
msgstr "web page"

#~ msgid "New"
#~ msgstr "New"

#~ msgid "_Find"
#~ msgstr "_Find"

#~ msgid "An elegant, distraction-free markdown editor"
#~ msgstr "An elegant, distraction-free markdown editor"

#~ msgid "Wolf V., Manuel G."
#~ msgstr "Wolf V., Manuel G."

#~ msgid "Apostrophe website"
#~ msgstr "Apostrophe website"

#~ msgid "Paypal"
#~ msgstr "PayPal"

#~ msgid "Help to translate:"
#~ msgstr "Help Translate:"

#~ msgid "Poeditor"
#~ msgstr "POEditor"

#~ msgid "CSS File"
#~ msgstr "CSS File"

#~ msgid "page0"
#~ msgstr "page0"

#~ msgid "page1"
#~ msgstr "page1"

#~ msgid "Sync Views"
#~ msgstr "Sync Views"

#~ msgid "Open Recent"
#~ msgstr "Open Recent"

#~ msgid "Save as…"
#~ msgstr "Save As…"

#~ msgid "Menu"
#~ msgstr "Menu"

#~ msgid "Autohide headerbar"
#~ msgstr "Autohide headerbar"

#~ msgctxt "shortcut window"
#~ msgid "Focus mode"
#~ msgstr "Focus Mode"

#~ msgctxt "shortcut window"
#~ msgid "Hemingway mode"
#~ msgstr "Hemingway mode"

#~ msgctxt "shortcut window"
#~ msgid "Find and replace"
#~ msgstr "Find and Replace"

#~ msgctxt "shortcut window"
#~ msgid "Cut selected text to clipboard"
#~ msgstr "Cut Selected Text"

#~ msgctxt "shortcut window"
#~ msgid "Paste selected text from clipboard"
#~ msgstr "Paste Text"

#~ msgid "(.*)"
#~ msgstr "(.*)"

#, python-format
#~ msgid "Save changes to document “%s” before closing?"
#~ msgstr "Save changes to document “%s” before closing?"

#~ msgid "Close without saving"
#~ msgstr "Close without Saving"

#~ msgid "Save now"
#~ msgstr "Save Now"

#~ msgid ""
#~ "Apostrophe is a GTK based distraction free Markdown editor, originally "
#~ "created by Wolf Vollprecht and maintained by Manuel Genovés. It uses "
#~ "pandoc as backend for markdown parsing and offers a very clean and sleek "
#~ "user interface."
#~ msgstr ""
#~ "Apostrophe is a distraction-free Markdown editor, originally created by "
#~ "Wolf Vollprecht and maintained by Manuel Genovés. It uses pandoc as a "
#~ "backend for markdown parsing and offers a very clean and sleek user "
#~ "interface."

#~ msgid "You can install the recommended TexLive extension with the command:"
#~ msgstr "You can install the recommended TexLive extension with the command:"

#~ msgid ""
#~ "flatpak install flathub org.gnome.gitlab.somas.Apostrophe.Plugin.TexLive"
#~ msgstr ""
#~ "flatpak install flathub org.gnome.gitlab.somas.Apostrophe.Plugin.TexLive"

#~ msgid "or from Gnome-Software"
#~ msgstr "or from Software"

#~ msgid "Apostrophe, a simple and distraction free Markdown Editor"
#~ msgstr "Apostrophe, a simple and distraction-free Markdown Editor"

#~ msgid "Enable or disable the dark mode."
#~ msgstr "Enable or disable the dark mode."

#~ msgid "Light mode isn’t available while using a dark global theme"
#~ msgstr "Light mode isn’t available while using a global dark theme"
